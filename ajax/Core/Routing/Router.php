<?php

namespace Codein\Core\Routing;

class Router
{
    // TO DO
    // const GET = 'GET';
    // const POST = 'POST';
    // const PUT = 'PUT';
    // const DELETE = 'DELETE';
    // const PATCH = 'PATCH';

    private static $_routes = [];

    // TO DO
    // private static $_routes = [
    //     'GET' => array(),
    //     'POST' => array(),
    //     'PUT' => array(),
    //     'DELETE' => array(),
    //     'PATCH' => array(),
    // ];

    public static function add(Route $route)
    {
        self::$_routes["wp_ajax_" . $route->getAction()] = $route->getCallback();
        if ($route->getPublic() === true) {
            self::$_routes["wp_ajax_nopriv_" . $route->getAction()] = $route->getCallback();
        }
        // TO DO
        // switch ($method) {
        //     case 'GET':
        //         self::$_routes["GET"]["wp_ajax_" . $route->getAction()] = $route->getCallback();
        //         if ($route->getPublic() === true) {
        //             self::$_routes["GET"]["wp_ajax_nopriv_" . $route->getAction()] = $route->getCallback();
        //         }
        //         break;
        //     case 'POST':
        //         self::$_routes["POST"]["wp_ajax_" . $route->getAction()] = $route->getCallback();
        //         if ($route->getPublic() === true) {
        //             self::$_routes["POST"]["wp_ajax_nopriv_" . $route->getAction()] = $route->getCallback();
        //         }
        //         break;
        //     case 'PUT':
        //         self::$_routes["PUT"]["wp_ajax_" . $route->getAction()] = $route->getCallback();
        //         if ($route->getPublic() === true) {
        //             self::$_routes["PUT"]["wp_ajax_nopriv_" . $route->getAction()] = $route->getCallback();
        //         }
        //         break;
        //     case 'DELETE':
        //         self::$_routes["DELETE"]["wp_ajax_" . $route->getAction()] = $route->getCallback();
        //         if ($route->getPublic() === true) {
        //             self::$_routes["DELETE"]["wp_ajax_nopriv_" . $route->getAction()] = $route->getCallback();
        //         }
        //         break;
        //     case 'PATCH':
        //         self::$_routes["PATCH"]["wp_ajax_" . $route->getAction()] = $route->getCallback();
        //         if ($route->getPublic() === true) {
        //             self::$_routes["PATCH"]["wp_ajax_nopriv_" . $route->getAction()] = $route->getCallback();
        //         }
        //         break;
        //     default:
        //         exit('ERROR!');
        //         break;
        // }
    }

    public static function ajax(Route $route)
    {
        self::add($route);
    }

    // TO DO
    // public static function get(Route $route)
    // {
    //     self::add($route, self::GET);
    // }

    // public static function post(Route $route)
    // {
    //     self::add($route, self::POST);
    // }

    // public static function put(Route $route)
    // {
    //     self::add($route, self::PUT);
    // }

    // public static function delete(Route $route)
    // {
    //     self::add($route, self::DELETE);
    // }

    // public static function patch(Route $route)
    // {
    //     self::add($route, self::PATCH);
    // }

    public static function run()
    {
        $routes = self::$_routes;
        foreach ($routes as $action => $method) {
            add_action($action, $method);
        }
        // var_dump($routes);
    }
}