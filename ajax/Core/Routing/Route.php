<?php

namespace Codein\Core\Routing;

use InvalidArgumentException;

class Route
{
    private $_action;
    private $_callback;
    private $_public;

    public function __construct(string $action, $callback, $public = false)
    {
        if (is_callable($callback)) {
            $this->_callback = $callback;
            return;
        }

        $this->_action = $action;
        $this->_public = $public;

        try {
            $this->_setCallback($callback);
        } catch (InvalidArgumentException $e) {
            exit('Error!');
        }
    }

    protected function _setCallback($callback)
    {
        $callback = (string)$callback;
        $aCallback = explode('@', $callback);

        if (is_callable($callback)) {
            $this->_callback = $callback;
        } else if (count($aCallback) == 2 && file_exists(get_template_directory() . '/controllers/' . $aCallback[0] . '.php')) {
            require_once get_template_directory() . '/controllers/' . $aCallback[0] . '.php'; // instead of including the class you can make usage of http://php.net/manual/en/function.spl-autoload-register.php
            $this->_callback[0] = new $aCallback[0];
            $this->_callback[1] = $aCallback[1];
        } else {
            throw new InvalidArgumentException('$callback is invalid.');
        }
    }

    public function getAction()
    {
        return $this->_action;
    }

    public function getCallback()
    {
        return $this->_callback;
    }

    public function getPublic()
    {
        return $this->_public;
    }
}