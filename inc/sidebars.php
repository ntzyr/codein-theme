<?php

add_action('widgets_init', function () {
    register_sidebars(
        1,  // number of sidebars
        array(
            'name' => esc_html__('Sidebar %d', 'codein-theme'),
            'id' => 'sidebar-1',
            'description' => esc_html__('Add widgets here.', 'codein-theme'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        )
    );
});