<?php

add_action('after_setup_theme', function () {
    register_nav_menus(array(
        'menu-1' => esc_html__('Primary', 'codein-theme'),
    ));
});